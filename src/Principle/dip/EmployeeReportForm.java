/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principle.dip;

import java.util.List;

/**
 *
 * @author admin
 */
public class EmployeeReportForm {

    private List<Employee> employees;
    private Employee selectedEmployee;
    private EmployeeReportInterface employeeReportInterface;

    public EmployeeReportForm(EmployeeReportInterface employeeReportInterface) {
        this.employeeReportInterface = employeeReportInterface;
    }

    public void getSelectedEmployeeReport() {
        this.employeeReportInterface.generateReport(selectedEmployee);
    }
}
